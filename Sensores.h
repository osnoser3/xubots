#ifndef SENSORES_H
#define SENSORES_H
#include "CSensoresLinea.h"
#include "CWirelessRX.h"
#include "Automata.h"

class Sensores {
	static Sensores *_instance;
	bool _cambio;
	CSensoresLinea *_sensoresLinea;
	CWirelessRX *_wireless;
	GamePad *_gamePad;
	Sensores();
public:
	static Sensores *instance();
	void actualizar(long);
	bool hayCambio();
	void asociar(GamePad*);
};

Sensores *Sensores::_instance = NULL;

Sensores *Sensores::instance() {
	return _instance == NULL ? (_instance = new Sensores) : _instance;
}

Sensores::Sensores() {
	int pines[] = { 23, 22, 21, 20 };
	_sensoresLinea = new CSensoresLinea(pines, 4);
	_wireless = new CWirelessRX(0);
	_gamePad = NULL;
}

bool Sensores::hayCambio() {
	return _cambio;
}

void Sensores::asociar(GamePad *gamePad) {
	_gamePad = gamePad;
}

void Sensores::actualizar(long tiempoEnMilisegundos) {
	_cambio = _sensoresLinea->actualizar(tiempoEnMilisegundos, _gamePad);
	if (_wireless->actualizar(tiempoEnMilisegundos))
	{
		_cambio = true;
	}
}

#endif