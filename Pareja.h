#ifndef PAREJA_H
#define PAREJA_H

template <class P, class S>
class Pareja
{
	P primero;
	S segundo;
public:
	Pareja(P&, S&);
	static Pareja de(P&, S&);
	P &getPrimero();
	S &getSegundo();
};

template <class P, class S>
Pareja<P, S>::Pareja(P &primero, S &segundo) : primero(primero), segundo(segundo) {

}

template <class P, class S>
Pareja<P, S> Pareja<P, S>::de(P &primero, S &segundo) {
	return Pareja(primero, segundo);
}

template <class P, class S>
P &Pareja<P, S>::getPrimero() {
	return primero;
}

template <class P, class S>
S &Pareja<P, S>::getSegundo() {
	return segundo;
}

#endif