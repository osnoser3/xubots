#ifndef C_SENSORES_LINEA_H
#define C_SENSORES_LINEA_H
#include "SensorLinea.h"
#include "WProgram.h"
#include "GamePad.h"

enum { IZQUIERDA, CENTRO_IZQUIERDA, CENTRO_DERECHA, DERECHA };

class CSensoresLinea
{
	 SensorLinea _sensor[4];
	 bool _activado;
public:
	CSensoresLinea(int[], int);
	~CSensoresLinea();
	bool actualizar(long, GamePad*);
	bool isActivado();
	void setActivado(bool);
};


CSensoresLinea::CSensoresLinea(int data[], int length) {
	_activado = true;
	// _sensor = new SensorLinea[4];
	for (int i = 0; i < length; ++i)
	{
		_sensor[i] = SensorLinea(data[i]);
	}
}

CSensoresLinea::~CSensoresLinea() {
	// delete(_sensor);
	// _sensor = NULL;
}

bool CSensoresLinea::actualizar(long tiempoEnMilisegundos, GamePad *gamePad) {
	if (!_activado)
	{
		return false;
	}
	//gamePad->setPress(GamePad::A, true);
	if(_sensor[CENTRO_IZQUIERDA].esNegro() && _sensor[CENTRO_DERECHA].esNegro()) {
		gamePad->setPress(GamePad::IZQUIERDA, false);
		gamePad->setPress(GamePad::DERECHA, false);
		gamePad->setPress(GamePad::A, true);
	} 
	if (_sensor[CENTRO_IZQUIERDA].esBlanco() && _sensor[CENTRO_DERECHA].esNegro()) {
		gamePad->setPress(GamePad::DERECHA, true);
		gamePad->setPress(GamePad::IZQUIERDA, false);
		gamePad->setPress(GamePad::A, true);
	} else if(_sensor[CENTRO_IZQUIERDA].esNegro() && _sensor[CENTRO_DERECHA].esBlanco()) {
		gamePad->setPress(GamePad::DERECHA, false);
		gamePad->setPress(GamePad::IZQUIERDA, true);
	 	gamePad->setPress(GamePad::A, true);
	}
	// if (_sensor[CENTRO_IZQUIERDA].esBlanco() && _sensor[CENTRO_DERECHA].esBlanco())
	// {
	// 	if (_sensor[DERECHA].anteriorEsNegro())
	// 	{
	// 		gamePad->setPress(GamePad::DERECHA, true);
	// 		gamePad->setPress(GamePad::IZQUIERDA, false);
	// 		gamePad->setPress(GamePad::A, true);
	// 	} else {
			
	// 	}
	// }
//	Serial.print("Sensor IZQUIERDA: ");
//	Serial.println(_sensor[IZQUIERDA].analogInput());
//	Serial.print("Sensor CENTRO_IZQUIERDA: ");
//	Serial.println(_sensor[CENTRO_IZQUIERDA].analogInput());
//	Serial.print("Sensor CENTRO_DERECHA: ");
//	Serial.println(_sensor[CENTRO_DERECHA].analogInput());
//	Serial.print("Sensor DERECHA: ");
//	Serial.println(_sensor[DERECHA].analogInput());
	return false;
}

bool CSensoresLinea::isActivado() {
	return _activado;
}

void CSensoresLinea::setActivado(bool activado) {
	_activado = activado;
}

#endif
