#include "Robot.h"

Robot *_robot;
long previousMillis = 0;
long interval = 60; 
Relacion _relacion[Robot::FINAL];
bool bandera = true;
Pareja<Pareja<GamePad::Botones, bool>, int> aux1[] = {
		L::de(L::de(GamePad::A, false), Robot::DETENIDO),
		L::de(L::de(GamePad::X), Robot::RETROCESO),
		L::de(L::de(GamePad::DERECHA), Robot::ACELERAR_DERECHA),
		L::de(L::de(GamePad::IZQUIERDA), Robot::ACELERAR_IZQUIERDA)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux2[] = {
		L::de(L::de(GamePad::A), Robot::ACELERAR),
		L::de(L::de(GamePad::X), Robot::RETROCESO),
		L::de(L::de(GamePad::DERECHA), Robot::DERECHA),
		L::de(L::de(GamePad::IZQUIERDA), Robot::IZQUIERDA)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux3[] = {
		L::de(L::de(GamePad::X, false), Robot::DETENIDO),
		L::de(L::de(GamePad::A), Robot::ACELERAR),
		L::de(L::de(GamePad::DERECHA), Robot::RETROCESO_DERECHA),
		L::de(L::de(GamePad::IZQUIERDA), Robot::RETROCESO_IZQUIERDA)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux4[] = {
		L::de(L::de(GamePad::A), Robot::ACELERAR_DERECHA),
		L::de(L::de(GamePad::DERECHA, false), Robot::DETENIDO)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux5[] = {
		L::de(L::de(GamePad::A), Robot::ACELERAR_IZQUIERDA),
		L::de(L::de(GamePad::IZQUIERDA, false), Robot::DETENIDO)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux6[] = {
		L::de(L::de(GamePad::A, false), Robot::DERECHA),
		L::de(L::de(GamePad::IZQUIERDA), Robot::ACELERAR_IZQUIERDA),
		L::de(L::de(GamePad::DERECHA, false), Robot::ACELERAR)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux7[] = {
		L::de(L::de(GamePad::A, false), Robot::IZQUIERDA),
		L::de(L::de(GamePad::DERECHA), Robot::ACELERAR_DERECHA),
		L::de(L::de(GamePad::IZQUIERDA, false), Robot::ACELERAR)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux8[] = {
		L::de(L::de(GamePad::X, false), Robot::DERECHA),
		L::de(L::de(GamePad::IZQUIERDA), Robot::RETROCESO_IZQUIERDA),
		L::de(L::de(GamePad::DERECHA, false), Robot::RETROCESO)
	};
	Pareja<Pareja<GamePad::Botones, bool>, int> aux9[] = {
		L::de(L::de(GamePad::X, false), Robot::IZQUIERDA),
		L::de(L::de(GamePad::DERECHA), Robot::RETROCESO_DERECHA),
		L::de(L::de(GamePad::IZQUIERDA, false), Robot::RETROCESO)
	};


void setup() 
{
	_relacion[Robot::ACELERAR] = Relacion(aux1, 4);
	_relacion[Robot::DETENIDO] = Relacion(aux2, 4);
	_relacion[Robot::RETROCESO] = Relacion(aux3, 4);
	_relacion[Robot::DERECHA] = Relacion(aux4, 2);
	_relacion[Robot::IZQUIERDA] = Relacion(aux5, 2);
	_relacion[Robot::ACELERAR_DERECHA] = Relacion(aux6, 3);
	_relacion[Robot::ACELERAR_IZQUIERDA] = Relacion(aux7, 3);
	_relacion[Robot::RETROCESO_DERECHA] = Relacion(aux8, 3);	
	_relacion[Robot::RETROCESO_DERECHA] = Relacion(aux9, 3);
	Automata::instance()->setRelacion(_relacion, Robot::FINAL);
}

void loop() 
{
	if(bandera) {
		delay(5000);
		bandera = false;
		_robot = new Robot;
	}
	// long currentMillis = millis();
	// if(currentMillis - previousMillis > interval) {
	// 	previousMillis = currentMillis;   
		_robot->actualizar(previousMillis);
	// }
}

