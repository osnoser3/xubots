#ifndef AUTOMATA_H
#define AUTOMATA_H
#include "Pareja.h"
#include "GamePad.h"

class Relacion;

class Automata
{
	Relacion *_relacion;
	int length;
	static Automata *_instance;
	Automata();
	// ~Automata();
public:
	static Automata *instance();
	Relacion *getRelacion();
	void setRelacion(Relacion*, int);
};

Automata *Automata::_instance = NULL;

Automata *Automata::instance() {
	return _instance == NULL ? (_instance = new Automata) : _instance;
}

Automata::Automata() { }

Relacion* Automata::getRelacion() {
	return _relacion;
}

void Automata::setRelacion(Relacion *relacion, int length) {
	_relacion = relacion;
	this->length = length;
}

class L
{
public:
	static Pareja<GamePad::Botones, bool> de(GamePad::Botones boton, bool press = true) {
		return Pareja<GamePad::Botones, bool>::de(boton, press);
	}

	static Pareja<Pareja<GamePad::Botones, bool>, int > de(Pareja<GamePad::Botones, bool> pareja, int estado) {
		return Pareja<Pareja<GamePad::Botones, bool>, int>::de(pareja, estado);
	}
};

class Relacion
{
	Pareja<Pareja<GamePad::Botones, bool>, int> *pareja;
	int length;

	public:
		Relacion(Pareja<Pareja<GamePad::Botones, bool>, int>*, int);
		Relacion();
		int getLength();
		Pareja<Pareja<GamePad::Botones, bool>, int> *getParejas();

};

Relacion::Relacion(Pareja<Pareja<GamePad::Botones, bool>, int> *pareja, int length) {
	this->pareja = pareja;
	this->length = length;
}

Relacion::Relacion() { }

int Relacion::getLength() {
	return length;
}

Pareja<Pareja<GamePad::Botones, bool>, int> *Relacion::getParejas() {
	return pareja;
}

#endif
