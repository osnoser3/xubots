#ifndef MOTOR_DIRECCION
#define MOTOR_DIRECCION
#include "WProgram.h"

class MotorDireccion
{
	int vccPin, gndPin, enablePin;
public:
	MotorDireccion(int, int, int);
	~MotorDireccion();
	void movimientoDerecha();
	void movimientoIzquierda();
	void reiniciar();
	void detener();
};

MotorDireccion::MotorDireccion(int vccPin, int gndPin, int enablePin) : vccPin(vccPin), gndPin(gndPin), enablePin(enablePin) {
	pinMode(vccPin, OUTPUT);
	pinMode(gndPin, OUTPUT);
	pinMode(enablePin, OUTPUT);
	digitalWrite(enablePin, HIGH);
}

void MotorDireccion::movimientoDerecha() {
	digitalWrite(vccPin, HIGH);
	digitalWrite(gndPin, LOW);
}

void MotorDireccion::movimientoIzquierda() {
	digitalWrite(vccPin, LOW);
	digitalWrite(gndPin, HIGH);
}

void MotorDireccion::reiniciar() {

}

void MotorDireccion::detener() { 
	digitalWrite(vccPin, LOW);
	digitalWrite(gndPin, LOW); 
}

#endif