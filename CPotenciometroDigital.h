#ifndef C_POTENCIOMETRO_DIGITAL_H
#define C_POTENCIOMETRO_DIGITAL_H
#include "WProgram.h"

class CPotenciometroDigital
{
	int ucPin, dcPin, digitalPin;
public:
	CPotenciometroDigital(int, int, int);
	void analogUp(int);
	void analogDown(int);
	void digital(int, int);
};

CPotenciometroDigital::CPotenciometroDigital(int ucPin, int dcPin, int digitalPin) : ucPin(ucPin), dcPin(dcPin), digitalPin(digitalPin) {
	pinMode(ucPin, OUTPUT);
	pinMode(dcPin, OUTPUT);
	pinMode(digitalPin, OUTPUT);
}

void CPotenciometroDigital::digital(int nroVeces, int salida) {
	for (int i = 0; i < nroVeces; ++i)
	{
		digitalWrite(digitalPin, salida);
	}
}

void CPotenciometroDigital::analogUp(int nroVeces) {

}

void CPotenciometroDigital::analogDown(int nroVeces) {

}

#endif