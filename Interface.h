#ifndef INTERFACE_H
#define INTERFACE_H

class Interface {
	virtual void actualizar(long tiempoEnMilisegundos) = 0;
	virtual void reiniciar() = 0;
};

#endif