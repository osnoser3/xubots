#ifndef MOTOR_PRINCIPAL_H
#define MOTOR_PRINCIPAL_H 
#include "WProgram.h"

class MotorPrincipal
{
	int vccPin, gndPin, enablePin;
public:
	MotorPrincipal(int, int, int);
	// ~MotorPrincipal();
	void detener();
	void acelerar(int);
};

MotorPrincipal::MotorPrincipal(int vccPin, int gndPin, int enablePin) : vccPin(vccPin), gndPin(gndPin), enablePin(enablePin) {
	pinMode(vccPin, OUTPUT);
	pinMode(gndPin, OUTPUT);
	pinMode(enablePin, OUTPUT);
}

void MotorPrincipal::detener() {
	digitalWrite(enablePin, LOW);  
	digitalWrite(vccPin, LOW);
	digitalWrite(gndPin, LOW); 
}

void MotorPrincipal::acelerar(int direccion) {
	digitalWrite(enablePin, HIGH);  
	if (direccion == 1) { //ADELANTE
		digitalWrite(vccPin, HIGH);
		digitalWrite(gndPin, LOW);
		//analogWrite(gndPin, 255);  
	} else if(direccion == 2) { //RETROCESO
		digitalWrite(vccPin, LOW); 
		digitalWrite(gndPin, HIGH);
		//analogWrite(gndPin, 255);   
	} else {
		detener();
	}
}

#endif
