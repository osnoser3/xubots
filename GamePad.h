#ifndef GAME_PAD_H
#define GAME_PAD_H

class GamePad
{	
private:
	class EstadoBoton
	{
		bool activado, presionado;
	public:
		EstadoBoton(bool, bool);
		EstadoBoton();
		bool isActivado();
		bool isPresionado();
		void setActivado(bool);
		void setPresionado(bool);
	};
	
public:
	enum Botones { ARRIBA, ABAJO, IZQUIERDA, DERECHA,  A, B, X, Y, R1, R2, L1, L2, START, SELECT, NINGUNO };
	EstadoBoton boton[NINGUNO];
	GamePad();
	// ~GamePad();
	bool isPress(Botones);
	void setPress(Botones, bool);
	bool isActivated(Botones);
	void setActivated(Botones, bool);
};

GamePad::GamePad() {
	for (int i = 0; i < NINGUNO; ++i)
	{
		boton[i] = EstadoBoton(false, true);
	}
	boton[NINGUNO - 1].setPresionado(true);
}

GamePad::EstadoBoton::EstadoBoton() {
	presionado = activado = false;
}

GamePad::EstadoBoton::EstadoBoton(bool presionado, bool activado) {
	this->presionado = presionado;
	this->activado = activado;
}

bool GamePad::EstadoBoton::isPresionado() {
	return presionado;
}

bool GamePad::EstadoBoton::isActivado() {
	return activado;
}

void GamePad::EstadoBoton::setPresionado(bool presionado) {
	this->presionado = presionado;
}

void GamePad::EstadoBoton::setActivado(bool activado) {
	this->activado = activado;
}

bool GamePad::isPress(Botones boton) {
	return this->boton[boton].isPresionado();
}

bool GamePad::isActivated(Botones boton) {
	return this->boton[boton].isActivado();
}

void GamePad::setPress(Botones boton, bool presionado) {
	if(!this->boton[boton].isActivado()) return;
	this->boton[boton].setPresionado(presionado);
}

void GamePad::setActivated(Botones boton, bool activado) {
	this->boton[boton].setActivado(activado);
	if (!activado)
	{
		this->boton[boton].setPresionado(false);
	}
}

#endif
