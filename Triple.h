#ifndef TRIPLE_H
#define TRIPLE_H

template <class P, class S, class T>
class Triple
{
	P primero;
	S segundo;
	T tercero;
public:
	Triple(P&, S&);
	static Triple de(P&, S&, T&);
	P &getPrimero();
	S &getSegundo();
	T &getTercero();
};

template <class P, class S, class T>
Triple<P, S, T>::Triple(P &primero, S &segundo, T &tercero) : primero(primero), segundo(segundo), tercero(tercero) {

}

template <class P, class S, class T>
Triple<P, S, T> Triple<P, S, T>::de(P &primero, S &segundo, T &tercero) {
	return Triple(primero, segundo, tercero);
}

template <class P, class S, class T>
P &Triple<P, S, T>::getPrimero() {
	return primero;
}

template <class P, class S, class T>
S &Triple<P, S, T>::getSegundo() {
	return segundo;
}

template <class P, class S, class T>
T &Triple<P, S, T>::getTercero() {
	return tercero;
}
#endif