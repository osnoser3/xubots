#ifndef C_WIRELESS_RX_H
#define C_WIRELESS_RX_H
#include "WProgram.h"

class CWirelessRX
{
	int pinRX;
public:
	CWirelessRX(int);
	bool actualizar(long);
};

CWirelessRX::CWirelessRX(int pinRX) : pinRX(pinRX) {

}

bool CWirelessRX::actualizar(long tiempoEnMilisegundos) {
	return false;
}

#endif
