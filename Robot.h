#ifndef ROBOT_H
#define ROBOT_H
#include "Interface.h"
#include "Sensores.h"
#include "MotorDireccion.h"
#include "MotorPrincipal.h"

class Robot : Interface {
	public:
		enum Estado { INICIO, ADELANTE, RETROCESO, ACELERAR, RETROCESO_DERECHA, RETROCESO_IZQUIERDA, ACELERAR_DERECHA, ACELERAR_IZQUIERDA, DERECHA, IZQUIERDA, DETENIDO, FINAL };
		Robot();
		void actualizar(long);
		void reiniciar();
		bool comprobarAutomata();
		Estado getEstadoActual();
		void setEstadoActual(Estado);
		GamePad *getGamePad();
		bool press(Pareja<GamePad::Botones, bool>);
		void estadoInicio(long);
	private:
		Automata *_automata;
		Sensores *_sensores;
		MotorPrincipal *_motorPrincipal;
		MotorDireccion *_motorDireccion;
		GamePad *_gamePad;
		Estado _estadoActual;
		Estado _estadoAnterior;
		bool _cambio;
};

Robot::Robot() {
	_automata = Automata::instance();
	_gamePad = new GamePad;
	_sensores = Sensores::instance();
	_sensores->asociar(_gamePad);
	_motorPrincipal = new MotorPrincipal(4, 3, 11);
	_motorDireccion = new MotorDireccion(5, 6, 12);
	_motorDireccion->detener();
	_motorPrincipal->acelerar(ADELANTE);
	_cambio = true;
	_estadoActual = _estadoAnterior = ACELERAR;
}

GamePad *Robot::getGamePad() {
	return _gamePad;
}


Robot::Estado Robot::getEstadoActual() {
	return _estadoActual;
}

void Robot::setEstadoActual(Robot::Estado estado) {
	_estadoAnterior = _estadoActual;
	_estadoActual = estado;
}

void Robot::actualizar(long tiempoEnMilisegundos) {
	_sensores->actualizar(tiempoEnMilisegundos);
	_cambio = comprobarAutomata();
	Serial.print("Estado actual: ");
	Serial.println(_estadoActual);
	// if (_cambio)
	//  	return;
	switch(_estadoActual) {
		case INICIO:
			estadoInicio(tiempoEnMilisegundos);
			break;
		case ACELERAR:
			Serial.println("ACELERAR");
			_motorPrincipal->acelerar(ADELANTE);
			_motorDireccion->detener();
			break;
		case DETENIDO:
			Serial.println("DETENIDO");
			_motorDireccion->detener();
			_motorPrincipal->detener();
			break;
		case DERECHA:
			Serial.println("DERECHA");
			_motorDireccion->movimientoDerecha();
			_motorPrincipal->detener();
			break;
		case IZQUIERDA:
			Serial.println("IZQUIERDA");
			_motorDireccion->movimientoIzquierda();
			_motorPrincipal->detener();
			break;
		
		case ACELERAR_DERECHA:
			Serial.println("ACELERAR_DERECHA");
			_motorPrincipal->acelerar(ADELANTE);
			_motorDireccion->movimientoDerecha();
			break;
		case ACELERAR_IZQUIERDA:
			Serial.println("ACELERAR_IZQUIERDA");
			_motorPrincipal->acelerar(ADELANTE);
			_motorDireccion->movimientoIzquierda();
			break;
		case RETROCESO:
			Serial.println("RETROCESO");
			_motorPrincipal->acelerar(RETROCESO);
			_motorDireccion->detener();
			break;
		case RETROCESO_DERECHA:
			Serial.println("RETROCESO_DERECHA");
			_motorPrincipal->acelerar(RETROCESO);
			_motorDireccion->movimientoDerecha();
			break;
		case RETROCESO_IZQUIERDA:
			Serial.println("RETROCESO_IZQUIERDA");
			_motorPrincipal->acelerar(RETROCESO);
			_motorDireccion->movimientoIzquierda();
			break;
	}
}

bool Robot::comprobarAutomata() {
	Relacion relacion = _automata->getRelacion()[getEstadoActual()];
	Pareja<Pareja<GamePad::Botones, bool>, int> *parejas = relacion.getParejas();
	for (int i = 0, length = relacion.getLength(); i < length; ++i)
	{
		if (press(parejas[i].getPrimero()))
		{
			setEstadoActual(static_cast<Estado>(parejas[i].getSegundo()));
			return true;
		}
	}
	return false;
}

bool Robot::press(Pareja<GamePad::Botones, bool> botones) {
	return botones.getSegundo() ? _gamePad->isPress(botones.getPrimero()) : !_gamePad->isPress(botones.getPrimero());
}

void Robot::estadoInicio(long tiempoEnMilisegundos) {

}

void Robot::reiniciar() {

}

#endif
