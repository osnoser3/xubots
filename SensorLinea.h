#ifndef SENSORLINEA
#define SENSORLINEA_H
#include "WProgram.h"

class SensorLinea
{
	int min, max, media, sensorPin;
	bool estadoAnterior, estadoActual;
public:
	SensorLinea(int);
	SensorLinea();
	bool esNegro();
	bool esBlanco();
	bool anteriorEsBlanco();
	bool anteriorEsNegro();
	int analogInput();
	void calibrar();
};

SensorLinea::SensorLinea() { }

SensorLinea::SensorLinea(int sensorPin) : sensorPin(sensorPin) {
	pinMode(sensorPin, INPUT);
	min = 0;
	max = 1023;
	media = (max - min)/2 + min;
	estadoAnterior = estadoActual = false;
}

bool SensorLinea::esBlanco() {
	return !esNegro();
}

bool SensorLinea::esNegro() {
	estadoAnterior = estadoActual;
	return (estadoActual = analogRead(sensorPin) < media ? true : false); 
}

int SensorLinea::analogInput() {
	return analogRead(sensorPin);
}

bool SensorLinea::anteriorEsNegro() {
	return estadoAnterior;
}

bool SensorLinea::anteriorEsBlanco() {
	return !estadoAnterior;
}

void SensorLinea::calibrar() { }

#endif
